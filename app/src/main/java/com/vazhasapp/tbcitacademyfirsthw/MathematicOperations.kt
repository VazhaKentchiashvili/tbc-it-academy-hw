package com.vazhasapp.tbcitacademyfirsthw

class MathematicOperations() {

    // რიცხვის უდიდესი საერთო გამყოფი.
    fun ricxvisUsg(firstNumber: Int, secondNumber: Int): Int {

        var udidesiSaertoGamyofi = 0

        var gamyofi = 1

        while (gamyofi <= firstNumber && gamyofi <= secondNumber) {
            if (firstNumber % gamyofi == 0 && secondNumber % gamyofi == 0) {
                udidesiSaertoGamyofi = gamyofi
            }
            gamyofi++
        }
        println("უდიდესი საერთო გამყოფი რიცხვებისა $firstNumber და $secondNumber = $udidesiSaertoGamyofi")
        return udidesiSaertoGamyofi
    }

    // რიცხვის უმცირესი საერთო ჯერადი.
    fun ricxvisUsj(firstNumber: Int, secondNumber: Int): Int {

        var umciresiSaertoJeradi: Int = if (firstNumber > secondNumber) firstNumber else secondNumber

        while (true) {
            if (
                umciresiSaertoJeradi % firstNumber == 0 &&
                umciresiSaertoJeradi % secondNumber == 0) {
                println("უმცირესი საერთო ჯერადი რიცხვებისა:  $firstNumber და $secondNumber = $umciresiSaertoJeradi.")
                break
            } else {
                umciresiSaertoJeradi++
            }
        }
        return umciresiSaertoJeradi
    }

    // ფუნქცია ამოწმებს შეიცავს, თუ არა მითითებული ტექსტი "$"-ს ნიშანს.
    fun textCheck(enteredText: String) {
        if (enteredText.contains("$"))
            println("ტექსტი შეიცავს \"$\"-ს")
        else
            println("ტექსტი არ შეიცავს \"$\"")
    }

    // ფუნქცია აბრუნბებს ყველა ლუწი რიცხვის ჯამს 100-მდე.
    fun recursion(startNumber: Int, maximumNumber: Int): Int {
        var sumOfAllEvenNumbers = startNumber

        if (startNumber < maximumNumber) {
            sumOfAllEvenNumbers  += recursion(startNumber + 2, maximumNumber)
        }
        return sumOfAllEvenNumbers

    }

    // ფუქნცია შეაბრუნებს ჩაწოდებულ არგუმენტს მარტივი ალგორითმით.
    fun reverseNumber(enteredNumber: Int): Int {

        var mtlianiCifri = enteredNumber
        var shebrunebuliRicxvi = 0

        while (mtlianiCifri != 0) {
            val titoRicxvi = mtlianiCifri % 10

            shebrunebuliRicxvi = shebrunebuliRicxvi * 10 + titoRicxvi

            mtlianiCifri /= 10
        }
        println("თქვენს მიერ შეყვანილი რიცხვის შებრუნებულია: $shebrunebuliRicxvi")

        return shebrunebuliRicxvi
    }

    // ფუნქცია შეამოწმებს იკითხება, თუ არა სიტყვა იგივენაირად, თუ ჩვენ მას შევაბრუნებთ.
    fun checkPolidrom(enteredText: String) {

        if (enteredText.reversed() == enteredText) {
            println("სიტყვა: \"$enteredText\" პოლიდრომია.")
        } else {
            println("სიტყვა: \"$enteredText\" პოლიდრომი არ არის")
        }
    }
}